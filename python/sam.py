import sys
import os.path, time
import json
from datetime import datetime
import zipfile

from os import listdir
from os.path import isfile, isdir, join
from filecmp import dircmp
import shutil

SAM_VERSION = "0.1.0"
SPECIAL_FILE_REMOVED = "removed.sam"
SPECIAL_STATUS = "status.sam"
ARCHIVE_PREFIX = "arch_"
#========================================================
#some helper functions
def give_files(mypath):
	fs = []
	for f in listdir(mypath):
		if (isdir(join(mypath,f))):
			ks = give_files(join(mypath,f))
			fs.extend(ks)
		if isfile(join(mypath,f)) :
			fs.append(join(mypath,f))
	return fs
	
def safe_copy_file(src,dst):
	dstfldr = dst.replace(os.path.basename(dst),"")
	if not os.path.exists(dstfldr): os.makedirs(dstfldr)
	shutil.copyfile(src,dst)
	
def touch(fname, times=None):
    fhandle = open(fname, 'a')
    try:
        os.utime(fname, times)
    finally:
        fhandle.close()

#come from : http://stackoverflow.com/questions/510357/python-read-a-single-character-from-the-user
class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()
#========================================================

#Helper function to output and optionally pretty print JSON.
_JSON_ENCODER = json.JSONEncoder()
_JSON_ENCODER.indent = 4
_JSON_ENCODER.sort_keys = True
def tojson(python_object):  
  return _JSON_ENCODER.encode(python_object)
  

DIR_GOALS = "goals/"
DIR_RECENT = "recent"
DIR_MODIF = "modifs/"
DIR_ARCHIVES = "archives/"
class Modif:
	def __init__(self, _author, _modifs, _pathDir, _message):
		self.id = 0
		self.date = datetime.now()
		self.author = _author
		self.modification = _modifs
		self.pathDir = _pathDir
		self.message = _message
		self.refreshFromFile()
		self.archives = []
		
	def addArchive(self, _archive, _action):
		self.archives.append( _archive)
                
	def tojson(self):  
		r = {};
		r["VERSION"]=SAM_VERSION
		r["id"]=self.id
		r["date"]=str(self.date)
		r["author"]=self.author
		r["modification"]=self.modification
		r["message"]=self.message
		archs = []
		for x in self.archives:
			archs.append(x.tojson())
		r["archives"] = archs
		return tojson(r)
		
	def exportToFile(self, _filePath):
		f = open(_filePath,"w")
		f.write(self.tojson())
		f.close()
		
	def export(self):
		s = self.pathDir + str(self.id).zfill(6)
		print("export modif report to "+s)
		self.exportToFile(s)
		
	def isVersionFileCreated(self, version):
		return os.path.isfile(self.pathDir + str(version).zfill(6))

	def refreshFromFile(self):
		self.id = 0
		while(self.isVersionFileCreated(self.id)):
			self.id = self.id + 1

		
class Archive:
	def __init__(self, nameFile, _archivePath):
		self.nameFile = nameFile
		self.foName = os.path.dirname(self.nameFile)
		self.fiName = ARCHIVE_PREFIX+ os.path.basename(self.nameFile)
		self.currentVersion = 0
		self.parentVersion = 0
		self.removed = False
		self.archivePath = _archivePath
		self.refreshCurrentVersionFromFolder()
		self.lastModifiedPush = 0

		
	def tojson(self):  
		r = {};
		r["currentVersion"]=self.currentVersion
		r["parentVersion"]=self.parentVersion
		r["nameFile"]=self.nameFile
		r["removed"]=self.removed
		return tojson(r)
	
	def getFolderName(self):
		return os.path.join(self.foName, self.fiName)
	def getVersionName(self, version):
		return str(version).zfill(4)
	def isFolderCreated(self):
		return os.path.exists(os.path.join(self.archivePath  , self.getFolderName()))
	def createFolder(self):
		return os.makedirs(os.path.join(self.archivePath  , self.getFolderName()))
			
	def getVersionFolder(self, version):
		return (os.path.join(os.path.join(self.archivePath,  self.getFolderName()),self.getVersionName(version)))
	def isVersionFolderCreated(self, version):
		return os.path.exists(os.path.join(os.path.join(self.archivePath,  self.getFolderName()),self.getVersionName(version)))
	def createVersionFolder(self, version):
		return os.makedirs(os.path.join(os.path.join(self.archivePath,  self.getFolderName()),self.getVersionName(version)))

	def refreshCurrentVersionFromFolder(self):
		self.currentVersion = 0
		if not self.isFolderCreated():
			return
		print(self.getVersionFolder(self.currentVersion));
		while(self.isVersionFolderCreated(self.currentVersion + 1)):
			self.parentVersion = self.currentVersion
			self.currentVersion = self.currentVersion + 1
		if self.isRemoved:
			self.removed = False
		print( " get last version of archive "+str(self.currentVersion))
		
	def isRemoved(self):
		destPath2 = self.getVersionFolder(self.currentVersion)+"/"+SPECIAL_FILE_REMOVED
		if os.exists(destPath2):
			return true
		return false
		
	def remove(self):
		self.isRemoved = True
		self.currentVersion = self.currentVersion +1
		if not self.isVersionFolderCreated(self.currentVersion):
			self.createVersionFolder(self.currentVersion)
		touch(os.path.join(self.getVersionFolder(self.currentVersion), SPECIAL_FILE_REMOVED))
		
	def addOrUpdate(self, pathBaseFile):
		self.currentVersion = self.currentVersion +1
		if not self.isFolderCreated():
				self.createFolder()
		if not self.isVersionFolderCreated(self.currentVersion):
				self.createVersionFolder(self.currentVersion)
		destPath2 = os.path.join(self.getVersionFolder(self.currentVersion),self.fiName)
		fromPath2 = os.path.join(pathBaseFile,self.nameFile)
		print ("copy "+ fromPath2 + " to " + destPath2)
		shutil.copyfile(fromPath2, destPath2)
		#safe_copy_file( fromPath2, destPath2)

class Project:
	def __init__(self):
		self.name = ""
		self.path = ""
		self.samPath=""
		self.currentModif = {} 
		
	def create(self, _name, _path, _samPath):
		self.name = _name
		self.path = _path
		self.samPath = _samPath
		self.createDirectory( self.samPath)
		#self.createDirectory( self.samPath + DIR_WORKING)
		self.createDirectory( self.samPath + DIR_ARCHIVES)
		self.createDirectory( self.samPath + DIR_GOALS)
		self.createDirectory( self.samPath + DIR_RECENT)
		self.createDirectory( self.samPath + DIR_MODIF)
		self.createMeta(self.samPath+"/"+self.name+".samproj")

	#not tested
	def load(self,_projectFilePath):
		f = open(_projectFilePath,"r")
		d = fromjson(f.read())
		f.close()
		print (d)
	
	def createDirectory(self, _name):
		if not os.path.exists(_name):
			os.makedirs(_name)
						
	def tojson(self):  
		r = {};
		r["version"]=SAM_VERSION
		r["name"]=self.name
		r["path"]=self.path
		r["samPath"]=self.samPath
		r["files"]=[]
		return tojson(r)
	
	def fromjson(self, _json):  
		json = fromjson(_json)
		SAM_VERSION = json["version"]
		self.name = json["name"]
		self.path = json["path"]
		self.samPath = json["samPath"]
		self.files = []
		return json
		
	def createMeta(self, _filePath):
		f = open(_filePath,"w")
		f.write(self.tojson())
		f.close()
	def print_diff_files(self,dcmp):
		for name in dcmp.diff_files:
			print ("diff_file "+name)
		for sub_dcmp in dcmp.subdirs.values():
			print_diff_files(sub_dcmp)
							
	def give_files(self, mypath, recursive = True):
		fs = []
		for f in listdir(mypath):
			if (isdir(join(mypath,f)) and recursive):
				ks = give_files(join(mypath,f))
				fs.extend(ks)
			elif isfile(join(mypath,f)) :
				fs.append(f)
		return fs
	
	def give_dir(self, mypath):
		fs = []
		for f in listdir(mypath):
			s = join(mypath,f)
			if (isdir(s) and not isfile(s)):
				fs.append(join(mypath,f))
				ks = self.give_dir(os.path.join(mypath,f))
				fs.extend(ks)	
		return fs
	
	def checkForModif(self, rightToLeft = False):
		m = {}
		m["r"] = []
		m["a"] = []
		m["m"] = []
		listDirWork = self.give_dir(self.path)
		listDirWork = [os.path.relpath(x,self.path) for x in listDirWork if ".sam" not in x]
		#print("listDirWork = " + str(listDirWork))
		listDirRecent = self.give_dir(self.samPath + DIR_RECENT)
		listDirRecent = [os.path.relpath(x,self.samPath + DIR_RECENT) for x in listDirRecent]
		#print("listDirRecent = " + str(listDirRecent))

		listAddedDir = list(set(listDirWork) - set(listDirRecent))
		#print("listAddedDir = " + str(listAddedDir))
		listRemovedDir = list(set(listDirRecent) - set(listDirWork))
		#print("listRemovedDir = " + str(listRemovedDir))
		
		listAddedFiles = []
		k = self.give_files(self.path, False)
		listAddedFiles.extend(k)
		for d in listAddedDir:
			k = self.give_files(os.path.join(self.path, d), False)
			k = [os.path.join(d, e) for e in k]
			listAddedFiles.extend(k)
		print("listAddedFiles = " + str(listAddedFiles))
		
		listRemovedFiles = []
		k = self.give_files(self.samPath + DIR_RECENT, False)
		listRemovedFiles.extend(k)
		for d in listRemovedDir:
			k = self.give_files(os.path.join(self.samPath + DIR_RECENT, d), False)
			k = [os.path.join(d, e) for e in k]
			listRemovedFiles.extend(k)
		print("listRemovedFiles = " + str(listRemovedFiles))
		
		listDirCommon = list(set(listDirWork).intersection(set(listDirRecent)))
		listUpdatedFiles = []
		listDirCommon.append("/")
		#print("listDirCommon = " + str(listDirCommon))
		for d in listDirCommon:
			dcmp = dircmp(os.path.join(self.path,d), os.path.join(self.samPath + DIR_RECENT,d)) 
			dcmp.diff_files = [os.path.join(d, e) for e in dcmp.diff_files]
			listUpdatedFiles.extend(dcmp.diff_files)
		print("listUpdatedFiles = " + str(listUpdatedFiles))
		if rightToLeft:
			m["a"] = listRemovedFiles
			m["r"] = listAddedFiles
			m["m"] = listUpdatedFiles
		else:
			m["a"] = listAddedFiles
			m["r"] = listRemovedFiles
			m["m"] = listUpdatedFiles

		f = open(self.samPath + SPECIAL_STATUS,"w")
		f.write(tojson(m))
		f.close()
		return m

	def pull(self, force = False):
		fromPath = self.samPath + DIR_RECENT+"/"
		destPath = self.path
		self.currentModif = self.checkForModif(True)
		modif = self.currentModif
		if len(modif["a"]) + len(modif["r"]) + len(modif["m"]) > 0 :
			if not force :
			    print("Your working dir is outdated");
			    print("Pulling will do the folowing action :")
			print("adding " + str(len(modif["a"])) + " files.")
			print("deleting " + str(len(modif["r"])) + " files.")
			print("updating " + str(len(modif["m"])) + " files.")
			if not force : print("to process, run pull force")
			
			if force:
				for a in modif["a"]:
					safe_copy_file(os.path.join(fromPath,a), os.path.join(destPath,a))
				for a in modif["m"]:
					safe_copy_file(os.path.join(fromPath,a), os.path.join(destPath,a))
				for a in modif["r"]:
					os.remove(os.path.join(destPath,a))

				print("-----------------------------")
				print (str(len(modif["a"])) + " files added.")
				print (str(len(modif["m"])) + " files updated.")
				print (str(len(modif["r"])) + " files removed.")
				print("-----------------------------")
		else:
			print("no change detected : your working dir is up to date.")
			print("----")
		    

	def push(self, _message):
		destPath = self.samPath + DIR_RECENT+"/"
		fromPath = self.path
		modif = self.checkForModif()
		self.currentModif = modif
		if len(modif["a"]) + len(modif["r"]) + len(modif["m"]) > 0 :
			m = Modif("AUTHOR", modif, self.samPath + DIR_MODIF, _message)
			m.export()
		    
			for a in modif["a"]:
				safe_copy_file(os.path.join(fromPath,a), os.path.join(destPath,a))
				d = Archive(a, os.path.join(self.samPath,DIR_ARCHIVES))
				d.addOrUpdate(self.path)
				m.addArchive(d,"a")
			for a in modif["m"]:
				safe_copy_file(os.path.join(fromPath,a), os.path.join(destPath,a))
				d = Archive(a, os.path.join(self.samPath,DIR_ARCHIVES))
				d.addOrUpdate(self.path)
				m.addArchive(d,"a")	
			for a in modif["r"]:
				os.remove(os.path.join(destPath,a))
				d = Archive(a, os.path.join(self.samPath,DIR_ARCHIVES))
				d.remove()	
			
		print("-----------------------------")
		print (str(len(modif["a"])) + " files added.")
		print (str(len(modif["m"])) + " files updated.")
		print (str(len(modif["r"])) + " files removed.")
		print("-----------------------------")
		
	
	
	

if len(sys.argv) > 2:
    projPath = sys.argv[1]
    samPath = sys.argv[2]
    command = sys.argv[3]
    print("starting sam ...")
    #projPath = "C:/2014/Projets/SAM/test"
    #samPath =  projPath + "/.sam/"

    print("proj path : " + projPath)
    print("samPath : " + samPath)
    print("command : " + command)
    if command == "diff":
            p = Project()
            p.create("test",projPath, samPath)
            d = p.checkForModif()
            print(str(d))
            
    elif command == "push":
            p = Project()
            mess = ""
            for i in  range(4, len(sys.argv)):
                mess = mess + sys.argv[i]

            p.create("test",projPath, samPath )
            p.push(mess)
            
    elif command == "pull":
            p = Project()
            p.create("test",projPath, samPath)
            if len(sys.argv) == 5 and sys.argv[4]=="force":
                p.pull(True)
            else:
                p.pull()
		
# -------
# For Testing
# -------
if len(sys.argv) == 1:
    p = Project()
    projectPath = "C:/2014/Projets/SAM/clientWindows/SamWindowsClient/SamWindowsClient/bin/Debug/test/"
    projectSamPath =  projectPath + ".sam/"
    p.create("test",projectPath, projectSamPath)
    print("-------")
    print("--checkForModif")
    print(str(p.checkForModif()))
    print("-------")
    print("--pull")
    p.pull()
    print("-------")
    #print("--pull force")
    #p.pull(True)
    print("-------")
    print("--push")
    print(p.push("test message"))

      
